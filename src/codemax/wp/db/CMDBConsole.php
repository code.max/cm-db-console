<?php
namespace codemax\wp\db;

abstract class CMDBConsole
{
    
    /**
     *
     * @var string
     */
    
    /**
     *
     * @param string $query
     * @param mixed ...$args
     * @return array
     */
    static public function db_query( string $query, ...$args ): array
    {
        global $wpdb;
        return $wpdb->get_results( $wpdb->prepare( $query, $args ), ARRAY_N );
    }
    
    /**
     * 
     * @param string $term
     * @return array
     */
    static public function imageIdsByTerm( string $term ): array
    {
        $query =  <<<QUERYPOSTBYTERM
SELECT posts.ID
FROM
	`wp_posts` AS posts
	INNER JOIN `wp_term_relationships` AS term_rela ON posts.ID = term_rela.object_id
	INNER JOIN `wp_term_taxonomy` AS term_tax ON term_rela.term_taxonomy_id = term_tax.term_taxonomy_id
    INNER JOIN `wp_terms` AS terms ON term_tax.term_id = terms.term_id
WHERE
	terms.slug = %s
    AND posts.post_type = 'attachment'
    AND posts.post_mime_type LIKE 'image%'
QUERYPOSTBYTERM;
        $results = self::db_query( $query, $term );
        $ids = [];
        foreach ( $results as $row )
        {
            $ids[] = $row[ 0 ];
        }
        return $ids;
    }
    
}